import os

from setuptools import setup

LONG_DESC = open(os.path.join(os.path.dirname(__file__), "README.md")).read()

setup(
    name="mig",
    use_scm_version=True,
    setup_requires=['setuptools_scm'],
    description="LDAP Migration Manager",
    long_description=LONG_DESC,
    author="IFB Taskforce",
    license="GNU GENERAL PUBLIC LICENSE v2",
    url="",
    zip_safe=False,
    install_requires=[
        'click',
        'python-ldap',
        'PyYAML'
    ],
    classifiers=[
        "Development Status :: 3 - Alpha",
        "Intended Audience :: Developers",
        'Natural Language :: English',
        "Operating System :: OS Independent",
        "Programming Language :: Python",
        "Programming Language :: Python :: 3",
    ],
    py_modules=['mig'],
    entry_points={
        'console_scripts': [
            'mig = mig:mig'
        ]
    }
)
