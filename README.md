# Mig A tools to help Ldap Migration

## Command

- Help
```
mig --help
```

- Test Config && Ldap Server
```
mig --test
```

- Compare Schema
```
mig check-schema
```
it should show warning about missing objectclass and attribute in the target ldap server

- Import One User
```
mig import-users --uid <uid>
```
uid is the uid defined in the source ldap server

- Import All Users
```
mig import-users
```

## Debug

- Enable Debug Logging
```
mig --debug test
```
debug flag must be set before command name


## other example:

- show ldap data on server
```bash
# search user
mig show --uid fgerbes
mig show --uid fgerbes --isfrom
# search objectClass
mig show --objc posixGroup --isfrom
mig show --objc organizationalUnit
# serch group (by cn)
mig show --cn singlecell2020
mig show --cn singlecell2020 --isfrom
mig show --cn cnrs --isfrom
mig show --cn eureka --isfrom
# search by ou
mig show --ou projects
mig show --ou users
mig show --ou groups
mig show --ou ifremer --isfrom

# search by ldap filter
mig show --filt '(&(gidNumber=1000)(objectClass=posixGroup))' --isfrom

# pip with shell cmd
mig show --filt '(objectClass=ipHost)' --isfrom | grep '(' | cut -f2 -d',' | sort -u |cut -f2 -d '='
```

without --isfrom you search on target server
with --isfrom you search on source server

- import project
```
mig --debug import-groups --cn singlecell2020 --project
mig import-groups --project
```

- import user groups
```
mig --debug import-groups --cn fgerbes --user
mig import-groups --user
```

- import group
```
mig --debug import-groups --cn 'Domain Admins'
mig import-groups
```

- import aliases
```
mig --debug import-aliases --cn 'francois.gerbes'
mig import-aliases
```

- import ip Host
_need samba_
```
mig --debug import-iphost --uid 'sbr7-l091$'
mig --debug import-iphost --uid 'n74-dev$'
mig import-iphost
```


## Install

- Clone the repository

```
git clone repository_url.git
```

`cd` to the repository folder

- Create a python virtual environment for the project

```
python -m venv env
```

- Activate the python virtual environment

```
source env/bin/activate
```

- Install the package in development mode

```
pip install -e .
```
