#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import click
import copy
import ldap
import ldap.modlist
import ldap.schema
import ldap.dn
import logging
import os
import sys
from pprint import pformat

# import unicodedata
# import re
from yaml import load, Loader


@click.group()
@click.version_option()
@click.option('--debug', is_flag=True)
def mig(debug):
    _set_log_level(debug)
    _check_conf()
    _check_and_init_ldap()
    _get_ldap_schema_info()
    logging.info("Migrate LDAP from %s to %s",
                 config['ldap_from_server'],
                 config['ldap_to_server'])


@mig.command()
def test():
    logging.info("Test Ok")


@mig.command()
@click.option('--isfrom', is_flag=True, default=False)
@click.option('--uid', default=None)
@click.option('--cn', default=None)
@click.option('--ou', default=None)
@click.option('--objc', default=None)
@click.option('--filt', default=None)
def show(isfrom, uid, cn, ou, objc, filt):
    # todo factoryze
    if uid:
        from_uid = _get_entries_by_attr('uid', uid, is_from=isfrom)
        logging.info("Entries: %s", pformat(from_uid))
    if cn:
        from_cn = _get_entries_by_attr('cn', cn, is_from=isfrom)
        logging.info("Entries: %s", pformat(from_cn))
    if ou:
        from_ou = _get_entries_by_attr('ou:dn:', ou, is_from=isfrom)
        logging.info("Entries: %s", pformat(from_ou))
    if objc:
        from_objc = _get_entries_by_class(objc, is_from=isfrom)
        logging.info("Entries: %s", pformat(from_objc))
    if filt:
        from_objc = _get_entries_by_filter(filt, is_from=isfrom)
        logging.info("Entries: %s", pformat(from_objc))


@mig.command()
@click.option('--isfrom', is_flag=True, default=False)
def show_objc(isfrom):
    if isfrom:
        logging.info(str(ldap_schema['objc_from']))
    else:
        logging.info(str(ldap_schema['objc_to']))


@mig.command()
def check_schema():
    for ob in ldap_schema['objc_from']:
        if ob not in ldap_schema['objc_to']:
            logging.warn("Objectclass " + str(ob) + " not in target ldap")

    for at in ldap_schema['attr_from']:
        if at not in ldap_schema['attr_to']:
            logging.warn("Attribute " + str(at) + " not in target ldap")

    # todo: find which attr and objc are mandatory
    # and exit on error if they are missing


@mig.command()
@click.option('--uid', default=None)
@click.option('--outogroup', is_flag=True, default=False)
def import_users(uid, outogroup):
    ldap_filter = config['ldap_from_user_filter']

    if uid:
        ldap_filter = '(uid=' + uid + ')'

    if len(ldap_filter) > 0:
        _add_ou(config['ldap_to_user_ou'])

        from_users = _get_entries_by_filter(ldap_filter, is_from=True)
        for from_user in from_users:
            logging.debug('IMPORT USER: %s', str(from_user))
            _add_or_update_user(from_user, outogroup)


@mig.command()
@click.option('--project', is_flag=True, default=False)
@click.option('--user', is_flag=True, default=False)
@click.option('--cn', default=None)
def import_groups(cn, project, user):
    ldap_filter = config['ldap_from_group_filter']
    ldap_group_ou = config['ldap_to_group_ou']

    if project:
        ldap_filter = config['ldap_from_project_filter']
        ldap_group_ou = config['ldap_to_project_ou']

    if user:
        ldap_filter = config['ldap_from_user_groups_filter']
        ldap_group_ou = config['ldap_to_user_groups_ou']

    if cn:
        ldap_filter = '(&(objectClass=posixGroup)(cn=' + cn + '))'

    if len(ldap_filter) > 0:
        _add_ou(ldap_group_ou)

        from_groups = _get_entries_by_filter(ldap_filter, is_from=True)
        for from_group in from_groups:
            logging.debug('IMPORT GROUP/PROJECT: %s', str(from_group))
            _add_or_update_cn(from_group, ldap_group_ou)


@mig.command()
@click.option('--cn', default=None)
def import_aliases(cn):
    ldap_filter = config['ldap_from_aliases_filter']
    ldap_aliases_ou = config['ldap_to_aliases_ou']

    if cn:
        ldap_filter = '(&(objectClass=myPostfixAlias) (cn=' + cn + '))'

    if len(ldap_filter) > 0:
        _add_ou(ldap_aliases_ou)

        from_aliases = _get_entries_by_filter(ldap_filter, is_from=True)
        for from_aliase in from_aliases:
            logging.debug('IMPORT ALIASES: %s', str(from_aliase))
            _add_or_update_cn(from_aliase, ldap_aliases_ou)



@mig.command()
def import_samba():
    # begin with samba ou
    ldap_filter_ou = config['ldap_from_samba_ou']
    if len(ldap_filter_ou) > 0:
        from_samba_ou = _get_entries_by_filter(ldap_filter_ou, is_from=True)
        for from_ou in from_samba_ou:
            logging.debug('IMPORT OU: %s', str(from_ou))
            new_dn = _clean_base_dn(from_ou[0])
            _add_ou(new_dn)

    # next samba domain as it may be used by other entry
    ldap_filter_domain = config['ldap_from_samba_domain']
    if len(ldap_filter_domain) > 0:
        from_samba_domain = _get_entries_by_filter(ldap_filter_domain, is_from=True)
        for from_domain in from_samba_domain:
            logging.debug('IMPORT DOMAIN: %s', str(from_domain))
            new_dn = _clean_base_dn(from_domain[0])
            _add_or_update_by_dn(from_domain, new_dn)


    # next most of samba objc (samba groups included) (may re-import already imported ex: domain))
    for samba_class in config['ldap_from_samba_objc']:
        ldap_filter_class = '(objectClass=' + samba_class + ')'
        logging.debug('CLASS FILTER : %s', str(ldap_filter_class))
        from_samba_class = _get_entries_by_filter(ldap_filter_class, is_from=True)
        for from_class in from_samba_class:
            logging.debug('IMPORT CLASS: %s', str(from_class))
            new_dn = _clean_base_dn(from_class[0])
            _add_or_update_by_dn(from_class, new_dn)

    # last samba account, it should be only the samba admin and some computers not in ipHost
    ldap_filter_account = config['ldap_from_samba_account']
    if len(ldap_filter_account) > 0:
        from_samba_account = _get_entries_by_filter(ldap_filter_account, is_from=True)
        for from_account in from_samba_account:
            logging.debug('IMPORT ACCOUNT: %s', str(from_account))

            if 'uid' in from_account[1]:
                logging.debug('Find uid: %s', str(from_account[1]['uid']))
                new_ou = _get_base_ou(from_account)
                _add_ou(new_ou)
                # we need uid here to replace samba admin if an admin account already exist
                _add_or_update_uid(from_account, new_ou)
            else:
                logging.warning('Samba account without uid: %s', from_account[0])


@mig.command()
@click.option('--uid', default=None)
def import_iphost(uid):
    ldap_filter = config['ldap_from_iphost_filter']

    if uid:
        ldap_filter = '(&(objectClass=iphost)(uid=' + uid + '))'

    if len(ldap_filter) > 0:
        # todo find a cleaner way to do it
        # computers ou may be need for iphost
        _add_ou('ou=computers,' + config['ldap_to_base_dn'])

        from_iphosts = _get_entries_by_filter(ldap_filter, is_from=True)
        for from_iphost in from_iphosts:
            logging.debug('IMPORT IPHOSTES: %s', str(from_iphost))
            ldap_iphost_ou = _clean_iphost_ou(from_iphost[0])
            _add_ou(ldap_iphost_ou)
            if 'manager' in from_iphost[1]:
                dn_manager = from_iphost[1]['manager'][0].decode()
                from_iphost[1]['manager'] = [_clean_user_dn(dn_manager).encode()]
            _add_or_update_uid(from_iphost, ldap_iphost_ou)


def _get_base_ou(from_entry):
    new_dn = _clean_base_dn(from_entry[0])
    uid_str = 'uid='+ from_entry[1]['uid'][0].decode() + ','
    new_ou = new_dn.replace(uid_str, '')
    logging.debug('New ou: %s', new_ou)
    return new_ou


def _clean_base_dn(dn):
    new_dn = dn.replace(config['ldap_from_base_dn'],config['ldap_to_base_dn'])
    logging.debug('New dn: %s', new_dn)
    return new_dn


def _clean_user_dn(dn):
    new_dn = dn.split(',')[0] + ',' + config['ldap_to_user_ou']
    logging.debug('New dn: %s', new_dn)
    return new_dn


def _clean_iphost_ou(dn):
    ou_list = []
    dn_exp = set(ldap.dn.explode_dn(dn))
    dn_ou = list(filter(lambda x: (x.startswith('ou')), dn_exp))

    for ou in dn_ou:
        ou_name = ou.split('=')[1]
        if ou_name not in config['ldap_to_iphost_ou']:
            ou_list.append(ou_name)

    new_dn = dn
    for ou in ou_list:
        new_dn = new_dn.replace('ou='+ ou +',','')

    new_dn = _clean_base_dn(new_dn)

    new_ou = ','.join(new_dn.split(',')[1:])
    logging.info('New ou: %s', new_ou)

    return new_ou


# ou can't be dropped/re-created/updated
def _add_ou(dn):
    ou = dn.split(',')[0].split('=')[1] # todo what if not first split
    # cur_ou = _get_one_entry('ou', ou, objc='organizationalUnit')
    cur_ou = _get_one_entry_by_dn(dn)
    if len(cur_ou) == 0:
        logging.info("Add ou: %s", dn)
        _add_entry(dn, {'objectClass': ['organizationalUnit'.encode()],
                        'ou': [ou.encode()]})


# dn is different from other, as we already have the dn, so we don't need to create it
def _add_or_update_by_dn (from_entry, dn):
    filtered_entry = _filter_entry(from_entry)

    logging.info("#"*80)
    logging.info("From: %s", from_entry[0])
    logging.info("To: %s", dn)

    to_entry = _get_one_entry_by_dn(dn)
    _add_or_update(dn, to_entry, filtered_entry)

    return dn


def _add_or_update_cn(from_cn, to_ou):
    return _add_or_update_common(from_cn, to_ou, 'cn')


def _add_or_update_uid(from_uid, to_ou):
    return _add_or_update_common(from_uid, to_ou, 'uid')


def _find_the_good_attr_val(entry, attr):
    # todo: may check len(entry) too ... or not
    if len(entry[1][attr]) == 1:
        return entry[1][attr][0].decode()
    if len(entry[1][attr]) > 1:
        # find if one attr=attr_val is in the dn
        for attr_val in entry[1][attr]:
            if entry[0].startswith(attr + '=' + attr_val.decode()):
                return attr_val.decode()

    # hum what should we do if we are here ?
    logging.warning("Warning no good guess for %s %s", entry[0], attr)
    return entry[1][attr][0].decode()


def _add_or_update_common(from_entry, to_ou, attr, remove_old_uid=True):
    filtered_entry = _filter_entry(from_entry)
    # attr_val = filtered_entry[1][attr][0].decode()
    attr_val = _find_the_good_attr_val(from_entry, attr)
    dn = attr + '=' + attr_val + ',' + to_ou

    logging.info("#"*80)
    logging.info("From: %s", from_entry[0])
    logging.info("To: %s", dn)

    if dn == config['ldap_to_bind_dn']:
        logging.warning("Skip as the target is the binded user %s", dn)
        return dn, attr_val

    to_entry = _get_one_entry_by_dn(dn)

    # if dn does not match, check if finded with uid
    if len(to_entry) == 0 and attr == 'uid':
        finded_entry = _get_one_entry(attr, attr_val)
        if len(finded_entry) > 0:
            old_dn = finded_entry[0]
            if old_dn == config['ldap_to_bind_dn']:
                logging.warning("Skip as the old target is the binded user %s", old_dn)
                return dn, attr_val
            if len(finded_entry) > 0 and remove_old_uid:
                logging.warning("Drop old entry as uid finded: from %s to %s", old_dn, dn)
                _delete_entry(old_dn)

    _add_or_update(dn, to_entry, filtered_entry)

    return dn, attr_val


def _add_or_update_user(from_user, outogroup=False):
    dn, uid = _add_or_update_uid(from_user, config['ldap_to_user_ou'])

    if outogroup:
        for new_group in _get_old_ou_list(from_user[0], dn):
            _add_user_to_group(uid, new_group)


def _get_old_ou_list(old_dn, new_dn):
    ou_list = []

    dn_diff = set(ldap.dn.explode_dn(old_dn)).difference(ldap.dn.explode_dn(new_dn))
    ou_diff = list(filter(lambda x: (x.startswith('ou')), dn_diff))
    logging.info('Ou diff %s', str(ou_diff))

    for ou in ou_diff:
        ou_list.append(ou.split('=')[1])

    return ou_list


def _add_user_to_group(uid, cn):
    ou = config['ldap_to_group_ou'] # todo add a param for ou ?
    dn = "cn=" + cn + ',' + ou

    _add_ou(ou)

    cur_group = _get_one_entry_by_dn(dn)

    logging.debug("CUR ==> %s", str(cur_group))

    if len(cur_group) > 0:
        new_group = copy.deepcopy(cur_group)
        if 'memberUid' in new_group[1].keys():
            if uid.encode() not in new_group[1]['memberUid']:
                new_group[1]['memberUid'].append(uid.encode())
        else:
            new_group[1]['memberUid'] = [uid.encode()]
    else:
        new_group = (dn, {
            'objectClass': ['top'.encode(), 'posixGroup'.encode()],
            'cn': [cn.encode()],
            'gidNumber': [str(_get_next_gid_number()).encode()],
            'memberUid': [uid.encode()],
        })

    logging.debug("NEW ==> %s", str(new_group))

    _add_or_update(dn, cur_group, new_group)


def _get_next_gid_number():
    res = ldap_bind['to'].search_s(base=config['ldap_to_group_ou'],
                                   scope=ldap.SCOPE_SUBTREE,
                                   filterstr='(objectclass=posixGroup)',
                                   attrlist=['gidNumber'])

    if len(res) == 0:
        next = 10000
    else:
        gids = sorted([int(gid[1].get('gidNumber')[0]) for gid in res])
        next = gids[-1] + 10000 # if other group are created in future mig run

    return next


def _add_or_update(dn, cur_entry, new_entry):
    logging.info('Check: %s', dn)
    if len(cur_entry) > 0:
        _update_entry(dn, cur_entry[1], new_entry[1])
    else:
        _add_entry(dn, new_entry[1])


def _get_one_entry_by_dn(dn, is_from=False):
    lbind = 'to'
    if is_from:
        lbind = 'from'
    try:
        result = ldap_bind[lbind].search_s(
            base=dn,
            scope=ldap.SCOPE_SUBTREE)
    except ldap.LDAPError as e:
        logging.debug(str(e))
        # sys.exit(1)
        # if dn is not find, it throw an error
        # todo : should check if this is another error
        result = []

    if len(result) > 0:
        logging.debug("FIND: %s ENTRY: %s", dn, str(result[0]))
        return result[0]  # return the first finded

    logging.debug("NOT FIND: %s", dn)
    return []  # return empty if no result


def _get_one_entry(attr, value, is_from=False, objc=None):
    result = _get_entries_by_attr(attr, value, is_from, objc)
    if len(result) > 0:
        logging.debug("FIND: %s:%s ENTRY: %s", attr, value, str(result[0]))
        return result[0]  # return the first finded

    logging.debug("NOT FIND: %s:%s", attr, value)
    return []  # return empty if no result


def _get_entries_by_class(objc, is_from=False):
    filt = '(objectClass=' + objc + ')'
    return _get_entries_by_filter(filt, is_from)


def _get_entries_by_attr(attr, value, is_from=False, objc=None):
    filt = '(' + str(attr) + '=' + str(value) + ')'
    if objc:
        filt = '(&(objectClass=' + objc + ')' + filt + ')'
    return _get_entries_by_filter(filt, is_from)


def _get_entries_by_filter(filt, is_from=False):
    lbind = 'to'
    lbase = config['ldap_to_base_dn']
    if is_from:
        lbind = 'from'
        lbase = config['ldap_from_base_dn']
    try:
        logging.debug("Get Entries: %s %s %s ", filt, lbind, str(lbase))
        result = ldap_bind[lbind].search_s(
            base=lbase,
            scope=ldap.SCOPE_SUBTREE,
            filterstr=filt)
    except ldap.LDAPError as e:
        logging.error(str(e))
        sys.exit(1)

    return result


def _delete_entry(dn):
    logging.info("Delete: %s", dn)
    try:
        ldap_bind['to'].delete_s(dn)
    except ldap.LDAPError as e:
        logging.error(str(e))
        sys.exit(1)


def _update_entry(dn, cur_entry, new_entry):
    # ignore oldexistent should avoid remove any unexistent attribute in target ldap
    # todo : maybe add this option as param
    modlist = ldap.modlist.modifyModlist(cur_entry,
                                         new_entry,
                                         ignore_oldexistent=1)
    if len(modlist) > 0:
        logging.info("Update: %s", dn)
        logging.debug("MODYFY DN: %s MODLIST: %s", dn, str(modlist))

        for mod in modlist:
            if mod[1] == 'objectClass':
                logging.warn("ObjectClass modification detected for: %s %s", dn, str(mod[2]))
                _delete_entry(dn)
                _add_entry(dn, new_entry)
                return  # we don't want to modify after _add

        try:
            ldap_bind['to'].modify_s(dn, modlist)
        except ldap.LDAPError as e:
            logging.error(str(e))
            sys.exit(1)
    else:
        logging.debug("NO DIFF DN: %s MODLIST %s", dn, str(modlist))


def _add_entry(dn, entry):
    logging.info("Add: %s", dn)
    modlist = ldap.modlist.addModlist(entry)
    logging.debug("ADD DN: %s MODLIST: %s", dn, str(modlist))

    try:
        ldap_bind['to'].add_s(dn, modlist)
    except ldap.LDAPError as e:
        logging.error(str(e))
        sys.exit(1)


def _filter_entry(entry):
    filtered = copy.deepcopy(entry)
    objc = filtered[1]['objectClass']

    objc_to_remove = []
    for ob in objc:
        # logging.debug("CHECK: %s", ob)
        if ob.decode() not in ldap_schema['objc_to']:
            logging.debug("OBJC REMOVE: %s", ob)
            objc_to_remove.append(ob)
        else:
            logging.debug("OBJC KEEP: %s", ob)

    for obj in objc_to_remove:
        objc.remove(obj)

    attr = filtered[1]
    attr_to_remove = []
    for at in attr:
        if at not in ldap_schema['attr_to']:
            logging.debug("ATTR REMOVE: %s", at)
            attr_to_remove.append(at)
        else:
            logging.debug("ATTR KEEP: %s", at)

    for atr in attr_to_remove:
        attr.pop(atr)

    logging.debug("FILTERED: %s", str(filtered))
    return filtered


def _get_schema(url):
    try:
        subschemasubentry_dn, schema = ldap.schema.urlfetch(url)
    except ldap.LDAPError as e:
        logging.error(str(e))
        sys.exit(1)

    logging.info("Find Schema in %s for url %s",
                 str(subschemasubentry_dn),
                 str(url))

    return schema


def _get_schema_obj(schema, obj_type):
    obj_list = {}

    # todo: add a cache by obj_type
    oc_tree = schema.tree(obj_type)
    for oc in oc_tree.keys():
        objc = schema.get_obj(obj_type, oc)
        if objc is not None:
            # logging.debug("OID: %s ENTRY %s", str(objc.oid), str((objc.names, objc.must, objc.may, objc.sup, objc.obsolete)))c'esfaut sa
            # logging.debug("ADD OBJC: %s", str(objc.names))
            # logging.debug("URL: %s ADD OBJC: %s",url, str(objc.names))
            for on in objc.names:
                obj_list[on] = objc
                # objc_list.append(on)

    return obj_list


def _get_obj_and_attr(url):
    schema = _get_schema(url)

    objc_list = _get_schema_obj(schema, ldap.schema.ObjectClass).keys()
    attr_list = _get_schema_obj(schema, ldap.schema.AttributeType).keys()

    return sorted(set(objc_list)), sorted(set(attr_list))


def _get_ldap_schema_info():
    global ldap_schema
    ldap_schema = {}

    ldap_schema['objc_to'], ldap_schema['attr_to'] = _get_obj_and_attr(config['ldap_to_server'])
    ldap_schema['objc_from'], ldap_schema['attr_from'] = _get_obj_and_attr(config['ldap_from_server'])


def _check_and_init_ldap():
    global ldap_bind
    ldap_bind = {}

    try:
        logging.debug("Try %s from %s",
                      str(config['ldap_from_bind_dn']),
                      str(config['ldap_from_server']))
        ldap_bind['to'] = ldap.initialize(config['ldap_to_server'])
        ldap_bind['to'].simple_bind_s(config['ldap_to_bind_dn'],
                                      config['ldap_to_password'])

        logging.debug("Try %s to %s",
                      str(config['ldap_to_bind_dn']),
                      str(config['ldap_to_server']))
        ldap_bind['from'] = ldap.initialize(config['ldap_from_server'])
        ldap_bind['from'].simple_bind_s(config['ldap_from_bind_dn'],
                                        config['ldap_from_password'])

    except ldap.LDAPError as e:
        logging.error("Unable to access ldap server")
        logging.error(str(e))
        sys.exit(1)

    logging.info("Connect %s from %s",
                 str(config['ldap_from_bind_dn']),
                 str(config['ldap_from_server']))
    logging.info("Connect %s to %s",
                 str(config['ldap_to_bind_dn']),
                 str(config['ldap_to_server']))


def _check_conf():
    global config

    if os.path.exists('conf.yml'):
        config_path = 'conf.yml'
    else:
        logging.error("No config file found")
        sys.exit(1)

    config = load(open(config_path, 'r'), Loader=Loader)

    key_list = ['ldap_from_server',
                'ldap_from_password',
                'ldap_from_base_dn',
                'ldap_from_bind_dn',
                'ldap_from_user_filter',
                'ldap_from_project_filter',
                'ldap_from_group_filter',
                'ldap_from_user_groups_filter',
                'ldap_from_ou_filter',
                'ldap_to_server',
                'ldap_to_password',
                'ldap_to_base_dn',
                'ldap_to_bind_dn',
                'ldap_to_user_ou',
                'ldap_to_group_ou',
                'ldap_to_user_groups_ou',
                'ldap_to_project_ou']

    for key in key_list:
        if key not in config:
            logging.error("%s is mandatory in config file", key)
            sys.exit(1)


def _set_log_level(debug=False):
    log_format = '%(message)s'
    log_level = logging.INFO
    if debug:
        log_format = '%(levelname)s:%(funcName)s: %(message)s'
        log_level = logging.DEBUG

    logging.basicConfig(stream=sys.stdout, format=log_format, level=log_level)
